package mobileclass.huestyles.extensions
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.navigation.NavController
import com.mobileclass.huestyles.R
import java.lang.ref.WeakReference

private var toast: Toast? = null
fun NavController.goDetailPrenda(bundlePrenda: Bundle){
    navigate(R.id.detailPrendaFragment, bundlePrenda)
}

fun ViewGroup.inflate(layoutId:Int)=LayoutInflater.from(context).inflate(layoutId, this, false)!!

fun Any.toast(context: WeakReference<Context>){
    toast?.cancel()
    context.get()?.let {
        toast = Toast.makeText(it,this.toString(),Toast.LENGTH_SHORT)
        toast?.show()
    }
}

fun hideKeyBoard(activity: WeakReference<Activity>){
        activity.get()?.let{
        val imm = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        it.currentFocus?.run {
            imm.hideSoftInputFromWindow(windowToken,0)
        }
    }
}
