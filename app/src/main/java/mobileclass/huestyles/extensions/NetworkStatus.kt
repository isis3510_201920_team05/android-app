package mobileclass.huestyles.extensions

import android.net.ConnectivityManager
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.mobileclass.huestyles.R
import java.lang.ref.WeakReference

fun doIfInternetConnection(view: WeakReference<View>,connManager:WeakReference<ConnectivityManager>, block: () -> Unit){
    var network = connManager.get()?.activeNetworkInfo
     if(network != null &&  network.isConnected){
         block()
    }else{
         view.get()?.run{
             Snackbar.make(this, R.string.noInternetConn, Snackbar.LENGTH_INDEFINITE)
                 .setAction("OK"){
                     Snackbar.make(this, R.string.pleaseCheckNetwork, Snackbar.LENGTH_INDEFINITE)
                         .setAction("OK"){}.show()
                 }.show()

         }
    }
}