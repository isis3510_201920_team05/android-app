package mobileclass.huestyles.extensions

import android.util.Patterns

fun String.validEmail():Boolean{
   return if(this.contains("@")){
        Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }else{
        false
    }
}