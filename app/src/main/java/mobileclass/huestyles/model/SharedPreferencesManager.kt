package mobileclass.huestyles.model

import android.content.Context
import android.preference.PreferenceManager
import java.lang.ref.WeakReference

fun saveStringPreferences(value: String, key:String, context: WeakReference<Context>): Boolean{
    println("Saving preferences ${context.get()} &&&&::::")
    context.get()?.let {
        println("Saving preferences ${context.get()} &&&&::::")
        val prefs = PreferenceManager.getDefaultSharedPreferences(it)
        prefs.edit().apply {
            println("Saving preferences ${context.get()} &&&&::::")
            putString(key, value)
            println("Saving preferences ${context.get()} &&&&::::")
            apply()
        }
        println("Saving preferences " +getStringPreferences(context,Constants.USER))
        return true
    }
    return false

}

fun getStringPreferences(context: WeakReference<Context>,key: String):String{
    context.get()?.let {
        return PreferenceManager.getDefaultSharedPreferences(it)
            .getString(key, Constants.NO_VALUE)!!
    }
    return ""
}

