package mobileclass.huestyles.model

import java.io.Serializable

data class Wear(var name: String = "None", var src:String="", var trend: String ="All",
                var sizes:String ="None", var price:Int = 0, var color:String = "None",
                var gender:String ="All", var tags:String="None", var stores:ArrayList<Int> = ArrayList(),
                var id:Int = 0, var description:ArrayList<String> = ArrayList(), var liked:Int=0, var category:String="None"): Serializable