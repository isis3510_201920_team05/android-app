package mobileclass.huestyles.model

import java.io.Serializable

data class Store(var name: String, var lat:Double, var long: Double): Serializable