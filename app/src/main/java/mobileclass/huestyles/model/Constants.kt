package mobileclass.huestyles.model

object Constants {
    const val KEY_EMAIL = "email"
    const val KEY_PASSWORD = "password"
    const val KEY_IS_COLOR_BLINDNESS = "colorBlind"
    const val KEY_COLOR_BLINDNESS_TYPE = "colorBlindType"
    const val KEY_FAVORITES="favorites"
    const val KEY_STYLE_TYPE = "styleType"
    const val NO_VALUE = "no_value"
    const val KEY_NAME = "name"
    const val KEY_UID = "uid"
    const val KEY_ONBOARDING_FINISHED = "onBoardingFinished"
    const val USER_PROFILE_EVENT = "USER_PROFILE_EVENT"
    const val MAP_VIEW_EVENT = "MAP_VIEW_EVENT"
    const val HOME_EVENT = "HOME_EVENT"
    const val FAVORITES_EVENT = "FAVORITES_EVENT"
    const val DETAIL_PRENDA_EVENT = "DETAIL_PRENDA_EVENT"
    const val CATEGORIES_EVENT = "CATEGORIES_EVENT"
    const val USERS_TABLE = "users"
    const val WEAR_COLOR = "wear_color"
    const val WEAR_GENDER = "wear_gender"
    const val WEAR_NAME = "wear_name"
    const val WEAR_PRICE = "wear_price"
    const val WEAR_SIZES = "wear_sizes"
    const val WEAR_SRC = "wear_src"
    const val WEAR_STORES = "wear_stores"
    const val WEAR_TAGS = "wear_tags"
    const val WEAR_TRENDS = "wear_trends"
    const val WEAR_NO_COLORBLIND_DESCRIPTION = "no_color_blind_description"
    const val WEAR_COLORBLIND_DESCRIPTION = "color_blind_description"
    const val WEAR = "wear"
    const val USER = "user"
    const val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1
    const val MY_PERMISSIONS_REQUEST_MAPS_RECEIVE=3
    const val LAT = "lat"
    const val LON = "long"
    const val NAME = "name"
    const val CATEGORY = "category"
    const val COLOR = "color"
    const val DESCRIPTION = "description"
    const val GENDER = "gender"
    const val ID = "id"
    const val LIKED = "liked"
    const val PRICE = "price"
    const val SIZES = "sizes"
    const val SRC = "src"
    const val STORES = "stores"
    const val TAGS = "tags"
    const val TREND = "trend"
    const val PERMISSION_NFC_WRITE = 2

}


