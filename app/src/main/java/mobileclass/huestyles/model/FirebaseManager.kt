package mobileclass.huestyles.model

import android.os.Bundle
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.ref.WeakReference

private var TAG=""

fun addUser(user: WeakReference<User>, firebaseAnalytics: FirebaseAnalytics){
    val db = FirebaseFirestore.getInstance()
    user.get()?.run {
        // Add a new document with a generated ID
        db.collection("users")
            .document(uid).set(this)
            .addOnSuccessListener { documentReference ->
                val bundle = Bundle()
                bundle.putString(FirebaseAnalytics.Param.METHOD, FirebaseAnalytics.Event.SIGN_UP)
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle)
            }
            .addOnFailureListener { e ->
                println(e)
                Crashlytics.getInstance().crash()
            }
    }
}

suspend fun updateUser(uid:String, usuario: Map<String,Any>, firebaseAnalytics: FirebaseAnalytics, onSuccess: () -> Unit, onError: () -> Unit){
    val db = FirebaseFirestore.getInstance()
    val documentUser = db.collection("users").document(uid.trim())
    documentUser.update(usuario).addOnCompleteListener{
        onSuccess()
    }.addOnFailureListener{
        error ->
        println(error)
        onError()
    }
}

fun deleteUser(){

}

fun updateFavoritesUser(uid:String, wear:String, onSuccess: () -> Unit, onError: () -> Unit){
    val db = FirebaseFirestore.getInstance()
    val array= ArrayList<String>()
    array.add(wear)
    db.collection("users").document(""+uid.trim()).get()
        .addOnSuccessListener { document  ->
            if(document!=null){
                val favs = document.get("favorites").toString().replace("[","").replace("]","").replace("null","").split(",")
                for(fav in favs){
                    if(fav!="")
                        array.add(fav)
                }
                val usuario = HashMap<String, Any>()
                usuario.put(Constants.KEY_FAVORITES, array)
                db.collection("users").document(""+uid.trim()).update(usuario).addOnCompleteListener{
                    onSuccess()
                }.addOnFailureListener{
                        error ->
                    println(error)
                    onError()
                }
            }
        }
        .addOnFailureListener { exception ->
            Log.w(TAG, "Error getting document: ", exception)
        }


}
fun updateFavoritesUserUnlike(uid:String, wear:String, onSuccess: () -> Unit, onError: () -> Unit){
    val array= ArrayList<String>()
    val db = FirebaseFirestore.getInstance()
    db.collection("users").document(""+uid.trim()).get()
        .addOnSuccessListener { document  ->
            if(document!=null){
                val favs = document.get("favorites").toString().replace("[","").replace("]","").replace("null","").split(",")
                for(fav in favs){
                    if(fav!="" && fav!=wear)
                        array.add(fav)
                }
                val usuario = HashMap<String, Any>()
                usuario.put(Constants.KEY_FAVORITES, array)
                db.collection("users").document(""+uid.trim()).update(usuario).addOnCompleteListener{
                    onSuccess()
                }.addOnFailureListener{
                        error ->
                    println(error)
                    onError()
                }
            }
        }
        .addOnFailureListener { exception ->
            Log.w(TAG, "Error getting document: ", exception)
        }


}
fun postStores(stores:ArrayList<Store>, onSuccess: () -> Unit, onError: () -> Unit){
    val db = FirebaseFirestore.getInstance()
    for (i in 0..stores.size-1){
        val store = HashMap<String, Any>()
        store.put(Constants.LAT,stores.get(i).lat)
        store.put(Constants.LON,stores.get(i).long)
        store.put(Constants.NAME,stores.get(i).name)
        db.collection("stores")
            .document("${i+16}").set(store).addOnCompleteListener{
                onSuccess()
            }
            .addOnFailureListener { e ->
                println(e)
                Crashlytics.getInstance().crash()
            }
    }
}
fun postWears(wears:ArrayList<Wear>, onSuccess: () -> Unit, onError: () -> Unit){
    val db = FirebaseFirestore.getInstance()
    for (i in 0..wears.size-1){
        val wear = HashMap<String, Any>()
        wear.put(Constants.NAME,wears.get(i).name)
        wear.put(Constants.CATEGORY,wears.get(i).category)
        wear.put(Constants.COLOR,wears.get(i).color)
        wear.put(Constants.DESCRIPTION,wears.get(i).description)
        wear.put(Constants.GENDER,wears.get(i).gender)
        wear.put(Constants.ID,wears.get(i).id)
        wear.put(Constants.LIKED,wears.get(i).liked)
        wear.put(Constants.PRICE,wears.get(i).price)
        wear.put(Constants.SIZES,wears.get(i).sizes)
        wear.put(Constants.SRC,wears.get(i).src)
        wear.put(Constants.STORES,wears.get(i).stores)
        wear.put(Constants.TAGS,wears.get(i).tags)
        wear.put(Constants.TREND,wears.get(i).trend)
        db.collection("clothing")
            .document("${i+1919}").set(wear).addOnCompleteListener{
                onSuccess()
            }
            .addOnFailureListener { e ->
                println(e)
                Crashlytics.getInstance().crash()
            }
    }
}