package mobileclass.huestyles.model
import java.io.Serializable

data class User(var name: String ="",
                var uid:String="",
                var styleType: String ="All",
                var isColorBlind:Boolean=false,
                var colorBlindType:String="None",
                var email:String ="",
                var onBoardingFinished: Boolean = false,
                var favorites:List<String> = ArrayList(),
                var sex: String ="Other"): Serializable
