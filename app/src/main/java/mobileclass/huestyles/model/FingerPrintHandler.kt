package mobileclass.huestyles.model

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.CancellationSignal
import androidx.core.app.ActivityCompat
import com.mobileclass.huestyles.Manifest
import mobileclass.huestyles.extensions.toast
import java.lang.ref.WeakReference

@SuppressLint("ByteOrderMark")
class FingerPrintHandler(private val appContext: Context): FingerprintManager.AuthenticationCallback(){
    private lateinit var cancellationSignal: CancellationSignal

    fun startAuth(manager: FingerprintManager, cryptoObject: FingerprintManager.CryptoObject){
        cancellationSignal = CancellationSignal()
        if(ActivityCompat.checkSelfPermission(appContext,Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED){
            return
        }
        manager.authenticate(cryptoObject,cancellationSignal,0,this,null)
    }

    override fun onAuthenticationError(errMsgId: Int,
                                       errString: CharSequence) {

        ("Authentication error\n" + errString).toast(WeakReference(appContext))
    }

    override fun onAuthenticationHelp(helpMsgId: Int,
                                      helpString: CharSequence) {

        ("Authentication help\n" + helpString).toast(WeakReference(appContext))
    }

    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult?) {
        "Authentication Succed".toast(WeakReference(appContext))
        super.onAuthenticationSucceeded(result)
    }
}