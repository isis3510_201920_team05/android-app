package mobileclass.huestyles.view.login

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import com.mobileclass.huestyles.R
import mobileclass.huestyles.model.User
import kotlinx.android.synthetic.main.activity_choose_color_blind.*
import mobileclass.huestyles.extensions.doIfInternetConnection
import mobileclass.huestyles.model.Constants
import mobileclass.huestyles.model.getStringPreferences
import mobileclass.huestyles.model.saveStringPreferences
import java.lang.ref.WeakReference

class ChooseColorBlindTypeActivity : AppCompatActivity() {
    private lateinit var connManager: ConnectivityManager
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_color_blind)

        connManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        btnDeutenaropia.setOnClickListener {
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)){
                startActivity(startActivityTrends("Deutenaropia"))
            }
        }

        btnTritanopia.setOnClickListener {
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)){
                startActivity(startActivityTrends("Tritanopia"))
            }
        }

        btnMonochromacy.setOnClickListener {
            doIfInternetConnection( WeakReference(it),WeakReference(connManager)){
                startActivity(startActivityTrends("Monochromacy"))
            }
        }
    }

    private fun startActivityTrends(colorBlindType:String): Intent {
        finish()
        val gson = Gson()
        val userPrefe = gson.fromJson(getStringPreferences(WeakReference(this@ChooseColorBlindTypeActivity),Constants.USER),User::class.java)
        userPrefe.colorBlindType = colorBlindType
        saveStringPreferences(gson.toJson(userPrefe),Constants.USER,WeakReference(this@ChooseColorBlindTypeActivity))
        return Intent(this, ChooseTrendActivity::class.java)
    }
}
