package mobileclass.huestyles.view.login

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.mobileclass.huestyles.R
import mobileclass.huestyles.view.feed.FeedActivity
import kotlinx.android.synthetic.main.activity_choose_trend.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import mobileclass.huestyles.extensions.doIfInternetConnection
import mobileclass.huestyles.extensions.toast
import mobileclass.huestyles.model.*
import java.lang.ref.WeakReference
import java.util.HashMap
import kotlin.coroutines.CoroutineContext

class ChooseTrendActivity : AppCompatActivity(), CoroutineScope {
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var connManager: ConnectivityManager
    override val coroutineContext:CoroutineContext
        get() = Dispatchers.IO + job
    private lateinit var job: Job
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
        setContentView(R.layout.activity_choose_trend)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        connManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        btnBusiness.setOnClickListener {
            //TODO filter by the user preferences before startFeed
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)){
                startFeedActivity("Business")
            }
        }
        btnSemiCasual.setOnClickListener {
            //TODO filter by the user preferences before startFeed
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)) {
                startFeedActivity("Semi-Casual")
            }
        }

        btnCasual.setOnClickListener {
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)) {
                startFeedActivity("Casual")
            }
        }

        btnAlternative.setOnClickListener {
            doIfInternetConnection( WeakReference(it),WeakReference(connManager)) {
                startFeedActivity("Alternative")
            }
        }

        textSkipStep.setOnClickListener {
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)) {
                startFeedActivity("All")
            }
        }
    }

    private fun startFeedActivity(styleType: String) {
        val intent = Intent(this, FeedActivity::class.java)
        val gson = Gson()
        val userPrefe = gson.fromJson(getStringPreferences(WeakReference(this@ChooseTrendActivity),Constants.USER),User::class.java)
        userPrefe.styleType = styleType
        saveStringPreferences(gson.toJson(userPrefe),Constants.USER,WeakReference(this@ChooseTrendActivity))
        val usuario = HashMap<String, Any>()
        usuario.put(Constants.KEY_IS_COLOR_BLINDNESS, userPrefe.isColorBlind)
        usuario.put(Constants.KEY_COLOR_BLINDNESS_TYPE, userPrefe.colorBlindType)
        usuario.put(Constants.KEY_STYLE_TYPE, userPrefe.styleType)
        usuario.put(Constants.KEY_ONBOARDING_FINISHED,true)
        doIfInternetConnection(WeakReference(btnAlternative), WeakReference(connManager)){
            launch {
                updateUser(userPrefe.uid,usuario,firebaseAnalytics, onSuccess = {
                    getString(R.string.dataUpdatedSuccess).toast(WeakReference(this@ChooseTrendActivity))
                    userPrefe.onBoardingFinished = true
                    saveStringPreferences(gson.toJson(userPrefe),Constants.USER,WeakReference(this@ChooseTrendActivity))
                    finish()
                    startActivity(intent)
                }, onError = {
                    getString(R.string.noUpdated).toast(WeakReference(this@ChooseTrendActivity))
                    finish()
                    startActivity(intent)
                })
            }
        }

    }
}
