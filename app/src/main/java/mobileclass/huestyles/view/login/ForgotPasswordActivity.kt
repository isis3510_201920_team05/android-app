package mobileclass.huestyles.view.login

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.mobileclass.huestyles.R
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.coroutines.*
import mobileclass.huestyles.extensions.doIfInternetConnection
import mobileclass.huestyles.extensions.hideKeyBoard
import mobileclass.huestyles.extensions.validEmail
import java.lang.ref.WeakReference
import kotlin.coroutines.CoroutineContext

class ForgotPasswordActivity : AppCompatActivity(), CoroutineScope {

    private lateinit var txtEmail: EditText
    private lateinit var auth:FirebaseAuth
    private lateinit var connManager: ConnectivityManager

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job
    private lateinit var job: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        job = Job()
        init()
    }

    private fun init(){
        connManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        txtEmail=findViewById(R.id.inputEmail)
        auth=FirebaseAuth.getInstance()

        mainViewForgotPassword.setOnClickListener{
            hideKeyBoard(WeakReference(this@ForgotPasswordActivity))
        }
        btnSend.setOnClickListener {
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)){
                val email=txtEmail.text.toString()
                if(!email.isBlank() && email.validEmail()){
                    auth.sendPasswordResetEmail(email)
                        .addOnCompleteListener(this){
                                task ->
                            if(task.isSuccessful){
                                startActivity(Intent(this, ResetPasswordActivity::class.java))
                            }
                            else{
                                Toast.makeText(this, "Email could not be sent", Toast.LENGTH_LONG).show()
                            }
                        }
                }
            }

        }
        textRegister.setOnClickListener {
            finish()
            startActivity(Intent(this,RegisterActivity::class.java))
        }
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }
}
