package mobileclass.huestyles.view.login

import android.content.Context
import android.content.Intent
import android.content.Intent.EXTRA_USER
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.mobileclass.huestyles.R
import kotlinx.android.synthetic.main.activity_register.*
import com.google.android.material.snackbar.Snackbar
import android.net.ConnectivityManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.RadioButton
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_register.inputEmail
import kotlinx.android.synthetic.main.activity_register.inputPassword
import kotlinx.coroutines.*
import mobileclass.huestyles.extensions.doIfInternetConnection
import mobileclass.huestyles.extensions.hideKeyBoard
import mobileclass.huestyles.extensions.toast
import mobileclass.huestyles.extensions.validEmail
import mobileclass.huestyles.model.*
import java.lang.ref.WeakReference
import kotlin.coroutines.CoroutineContext


class RegisterActivity: AppCompatActivity(), CoroutineScope {

    private lateinit var auth:FirebaseAuth
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var connManager:ConnectivityManager
    private lateinit var db: FirebaseFirestore
    private var name =""
    private var email = ""
    private var password = ""

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job
    private lateinit var job: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }
    private fun init(){
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        auth=FirebaseAuth.getInstance()
        connManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        db = FirebaseFirestore.getInstance()
        job = Job()
        checkEnableRegisterButton()
        textLogin.setOnClickListener {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }

        mainViewRegister.setOnClickListener{
            hideKeyBoard(WeakReference(this))
        }

        btnRegister.setOnClickListener {
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)){
                if(inputEmail.text?.isBlank() != false || inputName.text?.isBlank() != false||inputEmail.text?.isBlank() != false){
                    Snackbar.make(btnRegister, R.string.pleseFillRegisterData, Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK"){}.show()
                }else{
                    launch{
                        createNewAccount()
                    }.apply { invokeOnCompletion { cancel() } }
                }
            }
        }
    }
    private suspend fun createNewAccount(){
        launch(Dispatchers.Main){
        bannerImage.visibility = View.GONE
        content.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
        }.apply { invokeOnCompletion { cancel() } }
        auth.createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener(this){
                task ->
                if(task.isSuccessful){
                    val user:FirebaseUser?=auth.currentUser
                    doIfInternetConnection(WeakReference(btnRegister),WeakReference(connManager)){
                        launch{
                            verifyEmail(user)
                        }.apply { invokeOnCompletion { cancel() } }
                    }
                    var usuario = WeakReference(User())
                    usuario.get()?.name = name
                    usuario.get()?.email = user?.email.toString()
                    usuario.get()?.uid = user?.uid.toString()
                    usuario.get()?.sex = findViewById<RadioButton>(radioGroup.checkedRadioButtonId).text.toString()
                    launch{
                        usuario.get()?.let{
                            registerBasicUser(it)
                            launch(Dispatchers.Main){
                                startActivity(action(it))
                            }.apply { invokeOnCompletion { cancel() } }
                        }
                    }
                }
                else{
                    launch(Dispatchers.Main){
                        progressBar.visibility = View.GONE
                        bannerImage.visibility = View.VISIBLE
                        content.visibility = View.VISIBLE
                    }.apply { invokeOnCompletion { cancel() } }
                    val network = connManager.activeNetworkInfo
                    if(network != null && network.isConnected){
                        if(task.isCanceled){
                            Snackbar.make(btnRegister, R.string.emailExistsErrorMessage, Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK"){}.show()
                        }else{
                            Snackbar.make(btnRegister, R.string.unexpectedError, Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK"){}.show()
                        }
                    }else{
                        Snackbar.make(btnRegister, R.string.noInternetConn, Snackbar.LENGTH_INDEFINITE)
                            .setAction("OK"){}.show()
                    }
                }
            }
    }

    private suspend fun verifyEmail(user:FirebaseUser?){
        user?.sendEmailVerification()
                ?.addOnCompleteListener(this){
                    task ->
                    if(task.isComplete){
                        Snackbar.make(
                            btnRegister,
                            R.string.emailSentMessage,
                            Snackbar.LENGTH_INDEFINITE
                        ).setAction("OK"){}.show()
                    }else{
                        Snackbar.make(
                            btnRegister,
                            R.string.unexpectedError,
                            Snackbar.LENGTH_INDEFINITE
                        ).setAction("OK"){}.show()
                    }
                }?.addOnFailureListener {
                it.message?.toast(WeakReference(this))
            }
    }

    private suspend fun action(usuario: User): Intent {
        val intent=Intent(this@RegisterActivity, ColorBlindDecisionActivity::class.java)
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD,FirebaseAnalytics.Event.SIGN_UP)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle)
        intent.flags =  Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        return intent
    }

    private suspend fun registerBasicUser(user: User){
        // Create a new user with a name, isColorBlind, ColorBlindType, styleType, uid
        addUser(WeakReference(user),firebaseAnalytics)
        saveStringPreferences(Gson().toJson(user),Constants.USER,WeakReference(this@RegisterActivity))
        saveStringPreferences(inputPassword.text.toString(),Constants.KEY_PASSWORD,WeakReference(this@RegisterActivity))
    }

    private fun checkEnableRegisterButton(){
        val registerWarcher = object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                name = inputName.text.toString().trim()
                email = inputEmail.text.toString().trim()
                password = inputPassword.text.toString().trim()
                btnRegister.isEnabled = !name.isBlank()&&!email.isBlank()&&!password.isBlank()&&email.validEmail()
                if(!email.isBlank() && !email.validEmail()) getString(R.string.pleaseValidEmail).toast(
                    WeakReference(this@RegisterActivity)
                )
            }

            override fun afterTextChanged(s: Editable?) {
            }
        }
        inputName.addTextChangedListener(registerWarcher)
        inputEmail.addTextChangedListener(registerWarcher)
        inputPassword.addTextChangedListener(registerWarcher)
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }
}