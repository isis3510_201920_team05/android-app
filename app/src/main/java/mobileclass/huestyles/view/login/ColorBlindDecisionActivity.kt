package mobileclass.huestyles.view.login

import android.content.Context
import android.content.Intent
import android.content.Intent.EXTRA_USER
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_color_blind_decision.*
import kotlinx.coroutines.*
import mobileclass.huestyles.extensions.doIfInternetConnection
import mobileclass.huestyles.model.*
import java.lang.ref.WeakReference
import kotlin.coroutines.CoroutineContext


class ColorBlindDecisionActivity : AppCompatActivity(), CoroutineScope{
    private lateinit var connManager: ConnectivityManager
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job
    private lateinit var job: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.mobileclass.huestyles.R.layout.activity_color_blind_decision)
        job = Job()
        init()
    }

    private fun init(){
        connManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        btnYes.setOnClickListener {
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)){
                launch{
                    startActivity(actionYes())
                }
            }
        }

        btnNo.setOnClickListener {
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)) {
                launch{
                    startActivity(actionNo())
                }
            }
        }
    }
    private fun actionYes(): Intent {
        val userPrefe = Gson().fromJson(getStringPreferences(WeakReference(this@ColorBlindDecisionActivity),Constants.USER),User::class.java)
        userPrefe.isColorBlind = true
        saveStringPreferences(Gson().toJson(userPrefe),Constants.USER,WeakReference(this@ColorBlindDecisionActivity))
        finish()
        return Intent(this, ChooseColorBlindTypeActivity::class.java)

    }

    private fun actionNo(): Intent {
        finish()
        return Intent(this, ChooseTrendActivity::class.java)
    }
}
