package mobileclass.huestyles.view.login

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mobileclass.huestyles.R
import kotlinx.android.synthetic.main.activity_reset_password.*
import mobileclass.huestyles.extensions.doIfInternetConnection
import mobileclass.huestyles.extensions.hideKeyBoard
import java.lang.ref.WeakReference

class ResetPasswordActivity : AppCompatActivity() {
    private lateinit var connManager: ConnectivityManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        connManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        btnSubmit.setOnClickListener {
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)){
                startActivity(Intent(this, MainActivity::class.java))
            }
        }
        mainViewResetPassword.setOnClickListener{
            hideKeyBoard(WeakReference(this))
        }
    }
}
