package mobileclass.huestyles.view.login

import android.annotation.TargetApi
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.biometrics.BiometricPrompt
import android.hardware.fingerprint.FingerprintManager
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyPermanentlyInvalidatedException
import android.security.keystore.KeyProperties
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.mobileclass.huestyles.Manifest
import com.mobileclass.huestyles.R
import com.squareup.okhttp.Dispatcher
import mobileclass.huestyles.view.feed.FeedActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.bannerImage
import kotlinx.android.synthetic.main.activity_main.content
import kotlinx.coroutines.*
import mobileclass.huestyles.extensions.doIfInternetConnection
import mobileclass.huestyles.extensions.hideKeyBoard
import mobileclass.huestyles.extensions.toast
import mobileclass.huestyles.extensions.validEmail
import mobileclass.huestyles.model.*
import mobileclass.huestyles.model.Wear
import java.io.IOException
import java.lang.Exception
import java.lang.ref.WeakReference
import java.security.*
import java.security.cert.CertificateException
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey
import kotlin.coroutines.CoroutineContext
import kotlin.random.Random


class MainActivity : AppCompatActivity(), CoroutineScope {

    private lateinit var auth: FirebaseAuth
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var connManager: ConnectivityManager
    private var TAG=""
    private var mail = ""
    private var password = ""
    private lateinit var db: FirebaseFirestore
    private lateinit var fingerprintManager: FingerprintManager
    private lateinit var keyGuardManager: KeyguardManager
    private lateinit var keyStore: KeyStore
    private lateinit var keyGenerator: KeyGenerator
    private lateinit var cryptoObject: FingerprintManager.CryptoObject
    private val KEY_NAME = "example_key"
    private lateinit var cipher: Cipher
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private lateinit var job: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        job = Job()
        db = FirebaseFirestore.getInstance()
        launch(Dispatchers.Main){
            initialization(savedInstanceState)
        }.apply { invokeOnCompletion { cancel() } }
    }

    private fun initialization(savedInstanceState: Bundle?){

        auth=FirebaseAuth.getInstance()
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN,savedInstanceState)
        connManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

//        launch {
//            if(getManagers()){
//                generateKey()
//                if(cipherInit()){
//                    cipher?.let {
//                        cryptoObject = FingerprintManager.CryptoObject(it)
//                    }
//
//                    val helper = FingerPrintHandler(this@MainActivity)
//
//                    if (fingerprintManager != null && cryptoObject != null) {
//                        helper.startAuth(fingerprintManager!!, cryptoObject!!)
//                    }
//                }
//            }
//        }
        val loginTextWatcher = object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mail = inputEmail.text.toString().trim()
                password = inputPassword.text.toString().trim()
                val blank = mail.isBlank()||password.isBlank()
                var validEmail = false
                if(!blank){
                    validEmail = mail.validEmail()
                    if(!validEmail) {
                        launch(Dispatchers.Main) {
                            getString(R.string.pleaseValidEmail).toast(WeakReference(this@MainActivity))
                        }.apply { invokeOnCompletion { cancel() } }
                    }
                }
                btnLogin.isEnabled = !blank && validEmail
            }
            override fun afterTextChanged(s: Editable?) {

            }
        }
        inputEmail.addTextChangedListener(loginTextWatcher)
        inputPassword.addTextChangedListener(loginTextWatcher)

        mainView.setOnClickListener(){
            hideKeyBoard(WeakReference(this@MainActivity))
        }

        if(getStringPreferences(WeakReference(this@MainActivity),Constants.USER)!= Constants.NO_VALUE) {
            launch {
                goToFeed()
            }
        }else{
            splash.visibility = View.GONE
            bannerImage.visibility = View.VISIBLE
            content.visibility = View.VISIBLE

        }

        textForgotPassword.setOnClickListener{
            startActivity(Intent(this@MainActivity, ForgotPasswordActivity::class.java))
        }

        textRegister.setOnClickListener{
            startActivity(Intent(this@MainActivity, RegisterActivity::class.java))
        }
        btnLogin.setOnClickListener {
            hideKeyBoard(WeakReference(this@MainActivity))
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)) {
                launch {
                    println("Going FEED &&&")
                    doLogin()
                }.apply { invokeOnCompletion { cancel() } }
            }
        }
    }

//        btnGenerateData.setOnClickListener {
//            it.doIfInternetConnection(connManager){
//                //generateData()
//            }
//        }


   private suspend  fun doLogin(){
        launch (Dispatchers.Main){
            splash.visibility = View.VISIBLE
            bannerImage.visibility = View.GONE
            content.visibility = View.GONE
        }.apply { invokeOnCompletion { cancel() } }
        auth.signInWithEmailAndPassword(mail.trim(), password.trim())
            .addOnSuccessListener{
                db.collection(Constants.USERS_TABLE).document(auth.currentUser?.uid!!).get()
                    .addOnSuccessListener {
                            document ->
                        var user = document.toObject(User::class.java)
                        launch {
                            saveStringPreferences(Gson().toJson(user),Constants.USER,WeakReference(this@MainActivity))
                            saveStringPreferences(inputPassword.text.toString(),Constants.KEY_PASSWORD,WeakReference(this@MainActivity))
                            goToFeed()
                        }
                    }.addOnFailureListener{
                            exception ->
                        launch(Dispatchers.Main) {
                            splash.visibility = View.GONE
                            bannerImage.visibility = View.VISIBLE
                            content.visibility = View.VISIBLE
                            exception.message?.toast(WeakReference(this@MainActivity))
                        }.apply { invokeOnCompletion { cancel() } }
                        exception.printStackTrace()
                    }
            }.addOnFailureListener{ e ->
                launch(Dispatchers.Main){
                    bannerImage.visibility = View.VISIBLE
                    content.visibility = View.VISIBLE
                    splash.visibility = View.GONE
                    e.message?.toast(WeakReference(this@MainActivity))
                }.apply { invokeOnCompletion { cancel() } }
                e.printStackTrace()
            }
    }

    private fun goToFeed(){
//        withContext(Dispatchers.IO){
            finish()
            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.METHOD, FirebaseAnalytics.Event.LOGIN)
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle)
            launch(Dispatchers.Main){
                splash.visibility = View.GONE
            }.apply { invokeOnCompletion { cancel() } }
            startActivity(Intent(this@MainActivity, FeedActivity::class.java))
//        }
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }
    private fun generateData(){
        GlobalScope.launch {
            val colores = listOf("Vivid Red","Charcoal","Orange","White","Militar Green","Grey","Black","Green and Black","Dark Blue","Yellow","Dark Blue","Black","Dark Green","Purple","Brown")
            val category = listOf("T-Shirt","Pants","Jacket","Suit","Dress","Skirt","Sweater")
            val gender = listOf("Mens", "Women", "Unisex")
            val srcsJackets = listOf("https://i.pinimg.com/236x/bc/44/82/bc448288477b6647b8b7d4d0ca0d1738.jpg","https://i.pinimg.com/236x/e4/8f/d2/e48fd211236713f14bd0180ad6ae2243.jpg","https://i.pinimg.com/564x/66/a9/41/66a94104050f77be751bd181f04640ac.jpg","https://i.pinimg.com/236x/62/ef/0e/62ef0ef0a417050d5ad95346b0566684.jpg", "https://i.pinimg.com/236x/29/2d/11/292d115fb9e83cc0ff08678c7f974fc6.jpg", "https://i.pinimg.com/236x/17/c4/1b/17c41b8e7db3f06ab5143740a4ce8b1b.jpg")
            val srcPants = listOf("https://i.pinimg.com/236x/be/e9/26/bee926dde2ff5d59daab7e7169006e2a.jpg", "https://i.pinimg.com/236x/8b/c9/07/8bc907558530716c9c4acc9bc68bcf2e.jpg", "https://i.pinimg.com/236x/f8/b6/7c/f8b67ceed52df0230ef559909ca14ac7.jpg", "https://i.pinimg.com/236x/20/e3/03/20e303ca89223ab7262db5701f1be05b.jpg", "https://i.pinimg.com/236x/54/d2/a6/54d2a6994c98ef1b4a61ad15fb780a4a.jpg", "https://i.pinimg.com/236x/c2/e6/d6/c2e6d632e50d3369e6e30ddc13f5f227.jpg", "https://i.pinimg.com/236x/53/0b/fd/530bfde2d9b3f8ba304eba04eab01234.jpg", "https://i.pinimg.com/236x/03/a3/ab/03a3abcc22b29f5addcb8837cc603d7f.jpg")
            val srcShirts = listOf("https://i.pinimg.com/236x/b0/3a/88/b03a881b7c22d9f17a8521a54444cd5b.jpg","https://i.pinimg.com/236x/b0/3a/88/b03a881b7c22d9f17a8521a54444cd5b.jpg", "https://i.pinimg.com/236x/18/6a/6b/186a6bf576d1fd4684f69065e15e4a72.jpg", "https://i.pinimg.com/236x/6d/76/29/6d76299dbec6d53e099f5e1a37f9ae35.jpg", "https://i.pinimg.com/236x/36/47/94/364794fa5fea9f396176795cf4d2eb03.jpg", "https://i.pinimg.com/236x/ca/ec/fb/caecfb5ed2ebb6075a4ddad64fe50676.jpg", "https://i.pinimg.com/236x/db/8d/8b/db8d8b7496faeab469713235bcbb7580.jpg", "https://i.pinimg.com/236x/ec/ba/9c/ecba9ccdd08134100fefcf0381734510.jpg", "https://i.pinimg.com/236x/d1/21/0f/d1210fc71f1829334d501c778accb758.jpg", "https://i.pinimg.com/236x/97/9d/4d/979d4d277fefdc2e41347e0923c1481e.jpg", "https://i.pinimg.com/236x/97/9d/4d/979d4d277fefdc2e41347e0923c1481e.jpg")
            val srcSuits = listOf("https://i.pinimg.com/236x/81/a5/68/81a5680d6aefbe95f4192fd7ea30b4bb.jpg", "https://i.pinimg.com/236x/77/22/f6/7722f69fb56c6499f33b2544bbe59285.jpg", "https://i.pinimg.com/236x/7b/34/41/7b34419f78d377c966c921b1408e700f.jpg", "https://i.pinimg.com/236x/5f/fd/92/5ffd9277e470b3a284ecc7a581d8d414.jpg", "https://i.pinimg.com/236x/98/73/59/98735909b051ab1c3102d3fe8e55f7c4.jpg", "https://i.pinimg.com/236x/98/73/59/98735909b051ab1c3102d3fe8e55f7c4.jpg", "https://i.pinimg.com/236x/fb/97/c7/fb97c7c245f144e52dc33b684668e4d7.jpg","https://i.pinimg.com/236x/fb/97/c7/fb97c7c245f144e52dc33b684668e4d7.jpg")
            val srcDresses = listOf("https://i.pinimg.com/236x/bf/1d/3e/bf1d3e71e52fc9a77692e774ca72bb5d.jpg", "https://i.pinimg.com/236x/bf/1d/3e/bf1d3e71e52fc9a77692e774ca72bb5d.jpg", "https://i.pinimg.com/236x/49/46/a7/4946a7646818d856733fc80c9d2570b6.jpg", "https://i.pinimg.com/236x/49/46/a7/4946a7646818d856733fc80c9d2570b6.jpg", "https://i.pinimg.com/236x/49/46/a7/4946a7646818d856733fc80c9d2570b6.jpg", "https://i.pinimg.com/236x/3d/17/33/3d1733ecdfb42b58c7ee6ab5dcecf11a.jpg", "https://i.pinimg.com/236x/86/99/7b/86997b9948737cbaa9adab972c76f94b.jpg", "https://i.pinimg.com/236x/5d/f7/c3/5df7c3793a72c16e527aa21cf1b3107d.jpg", "https://i.pinimg.com/236x/82/59/81/825981ad0ede944b202922f395652120.jpg", "https://i.pinimg.com/236x/82/59/81/825981ad0ede944b202922f395652120.jpg","https://i.pinimg.com/236x/c2/07/30/c207303a93eeb70b7b240eefc1d0c681.jpg")
            val srcSkirt = listOf("https://i.pinimg.com/236x/82/65/1f/82651f612b65ab925f4929409a941738.jpg", "https://i.pinimg.com/236x/82/65/1f/82651f612b65ab925f4929409a941738.jpg", "https://i.pinimg.com/236x/82/65/1f/82651f612b65ab925f4929409a941738.jpg", "https://i.pinimg.com/236x/47/37/cc/4737cce4a6f216b8d1f13c4d74374824.jpg", "https://i.pinimg.com/236x/cb/c4/45/cbc4455b9dd919aa9e6614a36f3417df.jpg", "https://i.pinimg.com/236x/cb/c4/45/cbc4455b9dd919aa9e6614a36f3417df.jpg", "https://i.pinimg.com/236x/cb/c4/45/cbc4455b9dd919aa9e6614a36f3417df.jpg", "https://i.pinimg.com/236x/7c/ae/f8/7caef821c93299a91d24628e872ca2b1.jpg", "https://i.pinimg.com/236x/19/a2/5a/19a25addca26bbcdd816ef39eb41a43b.jpg", "https://i.pinimg.com/236x/19/a2/5a/19a25addca26bbcdd816ef39eb41a43b.jpg", "https://i.pinimg.com/236x/19/a2/5a/19a25addca26bbcdd816ef39eb41a43b.jpg", "https://i.pinimg.com/236x/19/a2/5a/19a25addca26bbcdd816ef39eb41a43b.jpg", "https://i.pinimg.com/236x/0c/b4/02/0cb402701b8ce8ff99b797c0bf6f9638.jpg")
            val srcSweaters = listOf("https://i.pinimg.com/236x/0c/b4/02/0cb402701b8ce8ff99b797c0bf6f9638.jpg", "https://i.pinimg.com/236x/4e/4f/71/4e4f71d123786d488079065e0b071fe4.jpg", "https://i.pinimg.com/236x/4e/4f/71/4e4f71d123786d488079065e0b071fe4.jpg", "https://i.pinimg.com/236x/4e/4f/71/4e4f71d123786d488079065e0b071fe4.jpg", "https://i.pinimg.com/236x/17/51/eb/1751ebe8c2e2c0eba4df9655f5270b47.jpg", "https://i.pinimg.com/236x/17/51/eb/1751ebe8c2e2c0eba4df9655f5270b47.jpg", "https://i.pinimg.com/236x/b2/08/49/b2084960edf783681c8b77df17a91b6d.jpg", "https://i.pinimg.com/236x/b8/da/dc/b8dadc5e287c9355e7c09d492c553f61.jpg", "https://i.pinimg.com/236x/7f/71/58/7f715813a45d938ce373cf747236747d.jpg", "https://i.pinimg.com/236x/f7/4e/37/f74e378b7ae7e6e31493a94f20e79b9e.jpg","https://i.pinimg.com/236x/e0/2c/eb/e02ceb796e0730a91d29b31727b3dee4.jpg")
            val trend = listOf("Business", "Semi-Casual", "Casual", "Alternative")
            val tags = "New Collection, Fashion, Chill, Style"
            val description = listOf("No color blind: Lorem ipsum dolor sit amet consectetur adipiscing elit dapibus sed nisl, consequat cras ullamcorper platea varius hac feugiat accumsan venenatis neque egestas, leo habitasse viverra suscipit nunc curabitur magna mi lacus. Ridiculus porta fringilla rutrum nulla sagittis condimentum litora aliquet, enim porttitor mauris est quam hendrerit rhoncus ultricies, commodo habitasse maecenas gravida dapibus senectus tortor. Libero aliquet consequat sociosqu molestie litora ultricies ", "Deuteranopia: Sed nisl consectetur adipiscing elit dapibus Lorem Ipsum dolor sit amet, feugiat accumsan Cras consequat este egestas en lo por venir, ni Ullamcorper la calle de los diversos aplicación de la ley, leo habitasse tincidunt viverra suscipit ahora un gran, organizaciones de arte. Las orillas de la puerta del legado de César Chávez condimentum fringilla rutrum nulla sagittis RISUS ñeque, rhoncus ultricies quam hendrerit porttitor mauris est, commodo tortor habitasse aliquam gesta z", "Tritanopia: nisl consectetur adipiscing elit dapibus lorem ipsum dolor sit amet, feugiat accumsan Cras egestas consequat this in time to come, nor ullamcorper the street of the various law enforcement, leo habitasse tincidunt viverra suscipit now a great, art organizations. The shores of the gate of the Legacy of Cesar Chavez condimentum fringilla rutrum nulla sagittis risus neque, rhoncus ultricies quam hendrerit porttitor mauris est, commodo tortor habitasse aliquam gravida dapibus old age. ", "Monochromatic: Sed NISL consectetur adipiscing elit dapibus lorem ipsum dolor sit amet, feugiat accumsan Cras egestas consequat isso em tempo para vir, nem ullamcorper a rua dos vários aplicação da lei, leo habitasse tincidunt viverra suscipit agora um grande, organizações de arte. As margens do portão do Legacy of Cesar Chavez condimentum fringilla rutrum nulla sagittis risus neque, rhoncus ultricies quam hendrerit porttitor mauris est, commodo tortor habitasse aliquam gravida dapibus velhice. ")
            val sizes = listOf("XS","S","M","L","XL","XXL")
            val storeNames = listOf("Falabella Santa fe","Falabella Titan","Falabella Colina","Falabella Fontanar","Arturo Calle 122","Arturo Calle Titan","Carlos Nieto Palatino","Carlos Nieto Outlet", "Lacoste Andino", "Lacoste Gran Estación", "Mango Fontanar", "Mango Unicentro", "Studio F Santa fé", "Stradivarius Santa fé", "Stradivarius Colina", "La Martina Santa fé", "Polo Ralph Lauren Andino", "Levi´s Toberin", "Levi´s Santa fe", "Bershka Colina", "Bershka Fontanar", "Tommy Hilfiger Unicentro", "Tommy Hilfiger Andino", "American Eagle Outfitters Santa fe", "American Eagle Outfitters Titan", "Hugo Boss Gran Estación", "Hugo Boss Colina", "Preppy Unicentro", "Preppy Andino", "Armani Exchange Colina", "Armani Exchange Titan")
            val lats = listOf(4.762661, 4.694805, 4.733785, 4.884562, 4.701556, 4.694805, 4.715886, 4.674041, 4.666882, 4.648210, 4.884562, 4.701352, 4.762661, 4.762661, 4.73378, 4.7626615, 4.666969, 4.744438, 4.761705, 4.762661, 4.733785, 4.702317 ,4.666753, 4.762661, 4.694805, 4.645757, 4.732725, 4.701751, 4.666770, 4.732927, 4.694877)
            val longs = listOf(-74.045415, -74.086268, -74.066189,  -74.034482, -74.048098, -74.086268, -74.029033, -74.053325, -74.053871, -74.101460, -74.034482, -74.041966, -74.045415, -74.045415, -74.066189, -74.045415, -74.053512, -74.043722, -74.045875, -74.045415, -74.066189, -74.041454, -74.052751, -74.045415, -74.086268, -74.102865, -74.066519, -74.041821, -74.053622, -74.066349, -74.086452)
            val list:ArrayList<Wear> = ArrayList()
            for (y in 0..100){
                val color=colores.get(Random.nextInt(0, colores.size-1))
//                val cat=category.get(Random.nextInt(0, category.size-1))
                val cat="Sweater"
                val gen=gender.get(Random.nextInt(0, gender.size-1))
                val nam=gen+" "+color+" "+cat+" "+y
                var src=""
                if(cat=="T-Shirt"){
                    src=srcShirts.get(Random.nextInt(0,srcShirts.size-1))
                }
                else if(cat=="Pants"){
                    src=srcPants.get(Random.nextInt(0,srcPants.size-1))
                }
                else if(cat=="Jacket"){
                    src=srcsJackets.get(Random.nextInt(0,srcsJackets.size-1))
                }
                else if(cat=="Suit"){
                    src=srcSuits.get(Random.nextInt(0,srcSuits.size-1))
                }
                else if(cat=="Dress"){
                    src=srcDresses.get(Random.nextInt(0,srcDresses.size-1))
                }
                else if(cat=="Skirt"){
                    src=srcSkirt.get(Random.nextInt(0,srcSkirt.size-1))
                }
                else if(cat=="Sweater"){
                    src=srcSweaters.get(Random.nextInt(0,srcSweaters.size-1))
                }
//                val tr=trend.get(Random.nextInt(0,trend.size-1))
                val tr="Alternative"
                val listaTiendas = ArrayList<Int>()
                for(i in 0..3){
                    val tienda= Random.nextInt(1,47)
                    if(!listaTiendas.contains(tienda)){
                        listaTiendas.add(tienda)
                    }
                }
                var tallas=""
                for(i in 0..Random.nextInt(1,sizes.size)){
                    if(tallas.equals("")){
                        tallas=sizes.get(i)
                    }
                    else{
                        tallas=tallas+","+sizes.get(i)
                    }
                }
                val desc= ArrayList<String>()
                desc.addAll(description)
                list.add(Wear(nam,src,tr,tallas,Random.nextInt(24000,3000000),color,gen,tags,listaTiendas,y+1919,desc,0,cat))
                Log.w(TAG, "///////////////////////////////////////////////////////acá////////////////////////////////")
                Log.w(TAG, nam)
            }
//            postStores(list, onSuccess = {Log.w(TAG, "Pasó ")},onError = {Log.w(TAG, "Se jodió xd")})
            postWears(list, onSuccess = {Log.w(TAG, "Pasó ")},onError = {Log.w(TAG, "Se jodió xd")})
        }
    }

    @TargetApi(28)
    private fun getManagers():Boolean{
        keyGuardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        fingerprintManager = getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager

        if (keyGuardManager?.isKeyguardSecure == false) {
                "Lock screen security not enabled in Settings".toast(WeakReference(this))
            return false
        }
        if(ActivityCompat.checkSelfPermission(this,
            Manifest.permission.USE_FINGERPRINT) !=
                PackageManager.PERMISSION_GRANTED) {
                "Fingerprint authentication permission not enabled".toast(WeakReference(this))
            return false
        }

        if (fingerprintManager?.hasEnrolledFingerprints() == false) {
            "Register at least one fingerprint in Settings".toast(WeakReference(this))
            return false
        }
        return true
    }

    private fun generateKey(){
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore")
        }catch (e: Exception){
            e.printStackTrace()
        }

        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES,"AndroidKeyStore")
        }catch (e: NoSuchAlgorithmException){
            throw RuntimeException(
                "Failed to get KeyGenerator instance", e)
        }catch (e: NoSuchProviderException){
            throw RuntimeException("Failed to get KeyGenerator instance", e)
        }

        try {
            keyStore?.load(null)
            keyGenerator?.init(KeyGenParameterSpec.Builder(KEY_NAME,
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setUserAuthenticationRequired(true)
                .setEncryptionPaddings(
                    KeyProperties.ENCRYPTION_PADDING_PKCS7)
                .build())
            keyGenerator?.generateKey()
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(e)
        } catch (e: InvalidAlgorithmParameterException) {
            throw RuntimeException(e)
        } catch (e: CertificateException) {
            throw RuntimeException(e)
        } catch (e: IOException) {
            throw RuntimeException(e)
        }

    }

    private fun cipherInit(): Boolean {
        try {
            cipher = Cipher.getInstance(
                KeyProperties.KEY_ALGORITHM_AES + "/"
                        + KeyProperties.BLOCK_MODE_CBC + "/"
                        + KeyProperties.ENCRYPTION_PADDING_PKCS7)
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Failed to get Cipher", e)
        } catch (e: NoSuchPaddingException) {
            throw RuntimeException("Failed to get Cipher", e)
        }

        try {
            keyStore?.load(null)
            val key = keyStore?.getKey(KEY_NAME, null) as SecretKey
            cipher?.init(Cipher.ENCRYPT_MODE, key)
            return true
        } catch (e: KeyPermanentlyInvalidatedException) {
            return false
        } catch (e: KeyStoreException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: CertificateException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: UnrecoverableKeyException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: IOException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: InvalidKeyException) {
            throw RuntimeException("Failed to init Cipher", e)
        }
    }
}