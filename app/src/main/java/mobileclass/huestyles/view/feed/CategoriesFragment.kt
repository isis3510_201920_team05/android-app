package mobileclass.huestyles.view.feed


import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.mobileclass.huestyles.R
import kotlinx.android.synthetic.main.content_home_content.view.*
import mobileclass.huestyles.extensions.goDetailPrenda
import kotlinx.android.synthetic.main.feed_home_item.*
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.android.synthetic.main.fragment_categories.view.*
import listeners.RecyclerWearListener
import mobileclass.huestyles.extensions.doIfInternetConnection
import mobileclass.huestyles.model.Constants
import mobileclass.huestyles.model.Wear
import java.lang.ref.WeakReference

/**
 * A simple [Fragment] subclass.
 */
class CategoriesFragment : Fragment() {

    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private lateinit var _view:View
    private lateinit var clothingDbRef: CollectionReference
    private val list:ArrayList<Wear> = ArrayList()
    private lateinit var recycler: RecyclerView
    private lateinit var adapter: WearAdapter
    private var TAG=""
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var connManager: ConnectivityManager
    private lateinit var category:String
    private lateinit var tempDocument: QueryDocumentSnapshot
    var isLoading=false
    val limit =20

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _view = inflater.inflate(R.layout.fragment_categories, container, false)
        setUpClothingDB()
        recycler = _view.recyclerNew as RecyclerView
        setRecyclerView()
        getAllClothes()
        category="All"
        return _view
    }

    private fun setRecyclerView() {
        recycler.setHasFixedSize(true)
        recycler.itemAnimator= DefaultItemAnimator()
        val layoutManager = GridLayoutManager(context,2)
        adapter=(WearAdapter(list, object : RecyclerWearListener {

            override fun onLike(wear: Wear, adapterPosition: Int){
                doIfInternetConnection(WeakReference(textNameHomeItem),WeakReference(connManager)){
                    btnLikeHomeItem.setOnClickListener{
                    }
                }
            }
            override fun onClick(wear: Wear, position: Int) {
                doIfInternetConnection(WeakReference(textNameHomeItem),WeakReference(connManager)){
                    val bundle = Bundle()
                    bundle.putSerializable(Constants.WEAR,wear)
                    findNavController().goDetailPrenda(bundle)
                }
            }
        }))
        recycler.layoutManager= layoutManager
        recycler.adapter=adapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        btn20More2.visibility=View.GONE
        Progress2.visibility=View.VISIBLE
        super.onActivityCreated(savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())
        connManager = requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD,Constants.CATEGORIES_EVENT)
        firebaseAnalytics.run{logEvent(Constants.CATEGORIES_EVENT,bundle)}
        fragmentManager?.apply { this.beginTransaction().addToBackStack("categoriesFragment")}
        chipAllCategories.setOnClickListener{
            category="All"
            getAllClothes()
        }
        chipTShirts.setOnClickListener {
            category="T-Shirt"
            getClothesByCategory()
        }
        chipPants.setOnClickListener {
            category="Pants"
            getClothesByCategory()
        }
        chipJackets.setOnClickListener {
            category="Jacket"
            getClothesByCategory()
        }
        chipSuits.setOnClickListener {
            category="Suit"
            getClothesByCategory()
        }
        chipDresses.setOnClickListener{
            category="Dress"
            getClothesByCategory()
        }
        chipSkirts.setOnClickListener{
            category="Skirt"
            getClothesByCategory()
        }
        chipSweaters.setOnClickListener{
            category="Sweater"
            getClothesByCategory()
        }
        btn20More2.setOnClickListener {
            //TODO filter by the user preferences before startFeed
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)) {
                if(category=="All"){
                    getPageAll()
                }
                else if(!isLoading){
                    getPage()
                }
            }
        }
    }

    private fun setUpClothingDB() {
        clothingDbRef = db.collection("clothing")
    }

    private fun getAllClothes(){
        isLoading = true
        if(list.size!=0){
            list.clear()
        }
        var i = 0
        clothingDbRef.limit(20).get()
            .addOnSuccessListener { result  ->
                for (document in result ) {
                    val stores = ArrayList<Int>()
                    i++
                    stores.addAll(document.get("stores") as Collection<Int>)
                    val wear = document.toObject(Wear::class.java)
                    list.add(wear)
                    adapter.notifyDataSetChanged()
                    if (i == limit - 1) {
                        tempDocument = document
                    }
                }
                isLoading = false
                Progress2?.visibility=View.GONE
                btn20More2?.visibility=View.VISIBLE
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }

    }
    private fun getClothesByCategory(){

        if(list.size!=0){
            list.clear()
        }
        isLoading=true
        clothingDbRef.whereEqualTo("category", category).limit(20).get()
            .addOnSuccessListener { result  ->
                var i = 0
                for (document in result ) {
                    val stores = ArrayList<Int>()
                    stores.addAll(document.get("stores") as Collection<Int>)
                    val wear = document.toObject(Wear::class.java)
                    list.add(wear)
                    i++
                    if(i==limit-1){
                        tempDocument=document
                    }
                    adapter.notifyDataSetChanged()
                }
                isLoading=false
                Progress2.visibility=View.GONE
                btn20More2.visibility=View.VISIBLE
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }

    }
    private fun getPage(){
        btn20More2.visibility=View.GONE
        Progress2.visibility=View.VISIBLE
        isLoading=true
        clothingDbRef.whereEqualTo("category", category).startAfter(tempDocument).limit(20).get()
            .addOnSuccessListener { result ->
                var i = 0
                for (document in result) {
                    val stores = ArrayList<Int>()
                    stores.addAll(document.get("stores") as Collection<Int>)
                    val wear = document.toObject(Wear::class.java)
                    list.add(wear)
                    i++
                    if(i==limit-1){
                        tempDocument=document
                    }
                }
                Handler().postDelayed({
                    adapter.notifyDataSetChanged()
                    isLoading=false
                    Progress2.visibility=View.GONE
                    btn20More2.visibility=View.VISIBLE
                },5000)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }
    private fun getPageAll(){
        btn20More2.visibility=View.GONE
        Progress2.visibility=View.VISIBLE
        isLoading=true
        clothingDbRef.startAfter(tempDocument).limit(20).get()
            .addOnSuccessListener { result ->
                var i = 0
                for (document in result) {
                    val stores = ArrayList<Int>()
                    stores.addAll(document.get("stores") as Collection<Int>)
                    val wear = document.toObject(Wear::class.java)
                    list.add(wear)
                    i++
                    if(i==limit-1){
                        tempDocument=document
                    }
                }
                Handler().postDelayed({
                    adapter.notifyDataSetChanged()
                    isLoading=false
                    Progress2.visibility=View.GONE
                    btn20More2.visibility=View.VISIBLE
                },5000)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }
}
