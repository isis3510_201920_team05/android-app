package mobileclass.huestyles.view.feed

import android.content.pm.PackageManager
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.mobileclass.huestyles.R
import mobileclass.huestyles.model.Constants
import mobileclass.huestyles.model.getStringPreferences
import mobileclass.huestyles.model.Store
import android.location.Location
import androidx.core.app.ActivityCompat
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.PolylineOptions
import mobileclass.huestyles.extensions.toast
import java.lang.ref.WeakReference


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private lateinit var mMap: GoogleMap
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val stores: ArrayList<Int> =ArrayList()
    private val MAPBOX_TOKEN:String ="pk.eyJ1Ijoic3JvZHJpZ3Vlem0iLCJhIjoiY2sydGRjcHc0MWFnbjNkcWV0dHQ2bDk1eCJ9.teE9xZPg_XK8NeZueJ26ww"
    private var URL:String=""
    private lateinit var storesDbRef: CollectionReference
    private var TAG=""
    private lateinit var lastLocation: Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        setUpStoreDB()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        val bundle = Bundle()
        val string=getStringPreferences(WeakReference(this@MapsActivity),Constants.WEAR_STORES).replace("[","").replace("]","").replace(" ", "")
        val stringArray = ArrayList<String>()
        if(string.contains(",")) {
            stringArray.addAll(string.split(","))
            for(line in stringArray){
                stores.add(line.toInt())
            }
        }
        else{
            stores.add(string.toInt())
        }
        bundle.putString(FirebaseAnalytics.Param.METHOD,Constants.MAP_VIEW_EVENT)
        firebaseAnalytics.run{logEvent(Constants.MAP_VIEW_EVENT,bundle)}
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setUpMap(mMap)
    }

    private fun setUpMap(mMap: GoogleMap) {
        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            // 3
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 13.0f))
                getStores(mMap)
            }
        }
    }


    private fun getStores(googleMap: GoogleMap){
        mMap = googleMap
        var ya=stores.size
        var storeA: Store = Store("TEMP",38.90992,-77.03613)
        for(store in stores){
            storesDbRef.document(""+store).get()
                .addOnSuccessListener { document  ->
                    if(document!=null){
                        val store = Store(document.getString("name").toString(),document.get("lat") as Double, document.get("long") as Double)
                        val place = LatLng(store.lat, store.long)
                        val zoomLevel = 13.0f //This goes up to 21
                        mMap.addMarker(MarkerOptions().position(place).title(store.name))
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place, zoomLevel))
                        if(ya!=0){
                            ya--
                        }
                        if(ya==0){
                            storeA=store
                            calculateRoute(lastLocation.latitude, lastLocation.longitude, storeA.lat, storeA.long)
                        }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error getting document: ", exception)
                }
        }

    }
    private fun setUpStoreDB() {
        storesDbRef = db.collection("stores")
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE
            )
        }
    }
    private fun calculateRoute(latO:Double, longO:Double, latD:Double, longD:Double){
        URL="https://api.mapbox.com/directions/v5/mapbox/driving/" +
                "$longO,$latO;$longD,$latD?access_token=$MAPBOX_TOKEN&geometries=geojson"
        Log.w(TAG, "Este es el link:"+URL)
        val request = JsonObjectRequest(Request.Method.GET,URL,null,
            Response.Listener{
                Log.w(TAG, "Recibe respuesta del link ")
                val routes=it.getJSONArray("routes").getJSONObject(0)
                val geometry=routes.getJSONObject("geometry")
                val coordinates=geometry.getJSONArray("coordinates")
                var arraySteps=ArrayList<LatLng>()
                for (i in 0..coordinates.length()-1){
                    val lngLat = coordinates.getJSONArray(i)
                    val fLng = lngLat.getDouble(0)
                    val fLat = lngLat.getDouble(1)
                    arraySteps.add(LatLng(fLat,fLng))
                }
                traceRoute(arraySteps)
            },
            Response.ErrorListener{
                getString(R.string.CouldntMakeARoute).toast(WeakReference( this@MapsActivity))
            })
        val q= Volley.newRequestQueue(this)
        q.add(request)



//        NavigationRoute.builder(this)
//            .accessToken(MAPBOX_TOKEN)
//            .origin(origin)
//            .destination(destination)
//            .build()
//            .getRoute(object : Callback<DirectionsResponse> {
//                override fun onResponse(call: Call, response: Response<DirectionsResponse>) {
//
//                }
//
//                override fun onFailure(call: Call, t: Throwable) {
//
//                }
//            })

    }
    private fun traceRoute(array:ArrayList<LatLng>){
        Log.w(TAG, "LLega acá "+ array.toString())
        val polyline=PolylineOptions().color(Color.BLUE).width(10.0f)
            .clickable(true).addAll(array)
        mMap.addPolyline(polyline)
        val currentLatLng = LatLng(lastLocation.latitude, lastLocation.longitude)
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 13.0f))

    }
}
