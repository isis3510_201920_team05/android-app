package mobileclass.huestyles.view.feed


import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson

import com.mobileclass.huestyles.R
import mobileclass.huestyles.view.login.MainActivity
import mobileclass.huestyles.extensions.doIfInternetConnection
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.inputEmail
import kotlinx.coroutines.*
import mobileclass.huestyles.extensions.hideKeyBoard
import mobileclass.huestyles.extensions.toast
import mobileclass.huestyles.extensions.validEmail
import mobileclass.huestyles.model.*
import java.lang.ref.WeakReference
import java.util.HashMap
import kotlin.coroutines.CoroutineContext


/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment(), AdapterView.OnItemSelectedListener, CoroutineScope{

    private lateinit var connManager: ConnectivityManager
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val auth = FirebaseAuth.getInstance()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private lateinit var job: Job

    private var colorBlindType: String = "None"
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        connManager= requireActivity().applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        firebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())
        job = Job()
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //FireBase Analytics.
        fragmentManager?.apply { this.beginTransaction().addToBackStack("profileFragment")}
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD,Constants.USER_PROFILE_EVENT)
        firebaseAnalytics.run{logEvent(Constants.USER_PROFILE_EVENT,bundle)}

        //Get the current user in the phone
        val gson = Gson()
        //This is for filling the spinnig with the types of color blindness
        ArrayAdapter.createFromResource(requireContext(),R.array.colorBlind_Array,android.R.layout.simple_spinner_item).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerTypeColorBlind.adapter = adapter
            spinnerTypeColorBlind.onItemSelectedListener = this@ProfileFragment
        }
        val spinnerList = resources.getStringArray(R.array.colorBlind_Array)


        //Set the hideKeyBoard function to the view so when the user click on the activity it will close the keyboard
        mainViewProfile.setOnClickListener{
            hideKeyBoard(WeakReference(this@ProfileFragment.requireActivity()))
        }


        val userPrefe = gson.fromJson(getStringPreferences(WeakReference(requireContext()),Constants.USER),User::class.java)
        println("Saving preferences $userPrefe")
        launch {
            //Check if the user finished the onBoarding phase
            checkUserFinishOnBoarding(userPrefe,gson)

            launch(Dispatchers.Main){
                //Set the inputs field with the values in the userPreference
                colorBlindType = userPrefe.colorBlindType
                inputName.setText(userPrefe.name)
                inputEmail.setText(userPrefe.email)
                spinnerTypeColorBlind.setSelection(spinnerList.indexOf(colorBlindType))
                //Set to the update button the update code
                btnUpdate.setOnClickListener{
                    update(userPrefe)
                }

                //Button for logout
                btnLogout.setOnClickListener {
                    doIfInternetConnection(WeakReference(it),WeakReference(connManager)){
                        saveStringPreferences(Constants.NO_VALUE,Constants.USER,WeakReference(requireContext()))
                        saveStringPreferences(Constants.NO_VALUE,Constants.KEY_PASSWORD,WeakReference(requireContext()))
                        startActivity(Intent(context,MainActivity::class.java).run { addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)})
                        requireActivity().finish()
                    }
                }
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        colorBlindType = parent?.getItemAtPosition(position).toString()
        println("ON ITEM SELECTED &&&&::::")
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    private suspend fun checkUserFinishOnBoarding(userPrefe: User, gson:Gson){
        if(!userPrefe.onBoardingFinished){
            val usuario = HashMap<String, Any>()
            btnAcceptData.visibility = View.VISIBLE
            btnAcceptData.setOnClickListener {
                doIfInternetConnection(WeakReference(it),WeakReference(connManager)){
                    val email = inputEmail.text.toString().trim()
                    if(!email.isBlank() && email.validEmail()){
                        launch {
                            usuario.put(Constants.KEY_IS_COLOR_BLINDNESS,userPrefe.isColorBlind)
                            usuario.put(Constants.KEY_COLOR_BLINDNESS_TYPE, userPrefe.colorBlindType)
                            usuario.put(Constants.KEY_STYLE_TYPE, userPrefe.styleType)
                            usuario.put(Constants.KEY_ONBOARDING_FINISHED,true)
                            updateUser(userPrefe.uid,usuario,firebaseAnalytics, onSuccess = {
                                userPrefe.onBoardingFinished = true
                                saveStringPreferences(gson.toJson(userPrefe),Constants.USER,WeakReference(requireContext()))
                                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.TUTORIAL_COMPLETE,null)

                                launch(Dispatchers.Main){
                                    btnAcceptData.visibility = View.GONE
                                    Snackbar.make(btnAcceptData,R.string.dataUpdatedSuccess,Snackbar.LENGTH_INDEFINITE).setAction("OK"){}.show()
                                    inputEmail.isClickable = true
                                    inputName.isClickable = true
                                    spinnerTypeColorBlind.apply {
                                        isClickable = true
                                        isEnabled = true
                                    }
                                }

                            }, onError = {
                                launch(Dispatchers.Main){
                                    Snackbar.make(btnAcceptData,R.string.unexpectedError,Snackbar.LENGTH_INDEFINITE).setAction("OK"){}.show()
                                }
                            })
                        }
                    }else{
                        getString(R.string.pleaseValidEmail).toast(WeakReference( requireContext()))
                    }
                }
            }
        }else{
            inputEmail.isClickable = true
            inputName.isClickable = true
            spinnerTypeColorBlind.apply {
                isClickable = true
                isEnabled = true
            }
        }
    }

    private fun update(userPrefe: User){
        val email = inputEmail.text.toString().trim()
        val name = inputName.text.toString().trim()
        doIfInternetConnection(WeakReference(btnUpdate),WeakReference(connManager)){
            launch{
                if(auth.currentUser == null){
                    auth.signInWithEmailAndPassword(userPrefe.email, getStringPreferences(WeakReference(requireContext()),Constants.KEY_PASSWORD)).addOnSuccessListener {
                    }.addOnFailureListener{
                        it.printStackTrace()
                    }
                }
                if(!email.isBlank() && !name.isBlank()){
                    auth.currentUser?.run {
                        if(email != this.email){
                            this.updateEmail(email).addOnSuccessListener {
                                userPrefe.email = email
                            }.addOnFailureListener{
                                launch(Dispatchers.Main) {
                                    Snackbar.make(btnUpdate,R.string.errorUpdatingEmailAuth,Snackbar.LENGTH_INDEFINITE).setAction("OK"){}.show()
                                }
                            }
                        }
                    }
                    userPrefe.name = name
                    userPrefe.colorBlindType = colorBlindType
                    updateDataInPreferences(WeakReference(userPrefe))
                }else{
                    launch(Dispatchers.Main){
                        getString(R.string.verifyEmailName).toast(WeakReference(requireContext()))
                    }
                }
            }
        }
    }
    private fun updateDataInPreferences(userPrefe: WeakReference<User>){

            userPrefe.get()?.run {
                val usuario = HashMap<String, Any>()
                usuario.put(Constants.KEY_COLOR_BLINDNESS_TYPE,colorBlindType)
                usuario.put(Constants.KEY_NAME,name)
                usuario.put(Constants.KEY_EMAIL,email)
                val gson = Gson()
                val json = gson.toJson(this)
                launch {
                    updateUser(uid,usuario,firebaseAnalytics,
                        onSuccess = {
                            saveStringPreferences(json,Constants.USER,WeakReference(this@ProfileFragment.requireContext()))
                            getString(R.string.dataUpdatedSuccess).toast(WeakReference(this@ProfileFragment.requireContext()))
                        },onError = {
                            launch(Dispatchers.Main){
                                Snackbar.make(btnUpdate,R.string.noUpdated,Snackbar.LENGTH_INDEFINITE).setAction("OK"){}.show()
                            }
                        })
                }
        }
    }

    override fun onDetach() {
        job.cancel()
        super.onDetach()
    }

}
