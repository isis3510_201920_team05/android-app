package mobileclass.huestyles.view.feed

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.mobileclass.huestyles.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.feed_home_item.*
import kotlinx.android.synthetic.main.fullscreen_image_prenda.*
import mobileclass.huestyles.model.Constants

class FullscreenImagePrenda: DialogFragment() {
    private lateinit var builder: AlertDialog.Builder
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity.let {
            builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            builder.setView(inflater.inflate(R.layout.fullscreen_image_prenda, null))
            builder.create()
        }
    }

    override fun onStart() {
        super.onStart()
        val imageView = dialog?.findViewById<ImageView>(R.id.imgFullscreenPrenda)
        imageView?.let{
            arguments?.run{
                Picasso.with(requireContext()).load(this.getString(Constants.WEAR_SRC)).into(it)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.setCanceledOnTouchOutside(true)
        return super.onCreateView(inflater, container, savedInstanceState)
    }
}