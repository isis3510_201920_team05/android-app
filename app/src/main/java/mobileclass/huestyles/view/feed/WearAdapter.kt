package mobileclass.huestyles.view.feed

import android.net.ConnectivityManager
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.mobileclass.huestyles.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.feed_home_item.view.*
import listeners.RecyclerWearListener
import mobileclass.huestyles.extensions.inflate
import mobileclass.huestyles.extensions.toast
import mobileclass.huestyles.model.*
import mobileclass.huestyles.model.Wear
import java.lang.ref.WeakReference


import mobileclass.huestyles.view.feed.OnBottomReachedListener as OnBottomReachedListener1



interface OnBottomReachedListener {
    fun onBottomReached(position: Int) {

    }

}
class WearAdapter(private val list: ArrayList<Wear>, private val listener: RecyclerWearListener):RecyclerView.Adapter<WearAdapter.ViewHolder>(){
    private lateinit var connManager:ConnectivityManager
    private lateinit var onBottomReachedListener: OnBottomReachedListener1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=
        ViewHolder(parent.inflate(com.mobileclass.huestyles.R.layout.feed_home_item))

    override fun onBindViewHolder(holder: ViewHolder, position: Int){
        holder.bind(list[position], listener)
        if (position == list.size ){
            onBottomReachedListener.onBottomReached(position);

        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setOnBottomReachedListener(ponBottomReachedListener: OnBottomReachedListener1) {
        this.onBottomReachedListener = ponBottomReachedListener
    }

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){

        private var TAG=""

        fun bind(wear: Wear, listener: RecyclerWearListener)=with(itemView){
            textNameHomeItem.text=wear.name
            textPriceHomeItem.text="$ "+wear.price.toString()
            val gson = Gson()
            val userPrefe = gson.fromJson(getStringPreferences(WeakReference(context), Constants.USER), User::class.java)
            val arreglo:ArrayList<String> = ArrayList()
            for(i in userPrefe.favorites)
            {
                arreglo.add(i.trim())
            }
            if(wear.src.isEmpty()) {
                Picasso.with(context).load(R.drawable.clothes).into(imgHomeItem)
            }
            else {
                Picasso.with(context).load(wear.src).into(imgHomeItem)
                if(arreglo.contains(wear.name)){
                    Picasso.with(context).load(R.drawable.ic_like_active).into(btnLikeHomeItem)
                }
            }
            setOnClickListener{
                listener.onClick(wear, adapterPosition)
            }
            btnLikeHomeItem.setOnClickListener{
                listener.onLike(wear, adapterPosition)
                if(!arreglo.contains(wear.name.trim())){
                    Picasso.with(context).load(R.drawable.ic_like_active).into(btnLikeHomeItem)
                    val uid=userPrefe.uid
                    updateFavoritesUser(uid, wear.name,onSuccess = {
                        context.getString(R.string.liked).toast(WeakReference(context))
                        arreglo.add(wear.name.trim())
                        userPrefe.favorites=arreglo
                        saveStringPreferences(Gson().toJson(userPrefe), Constants.USER, WeakReference(context))
                    },onError = {
                        Toast.makeText(context,"There was an error", Toast.LENGTH_SHORT)
                    })
                }
                else{
                    Picasso.with(context).load(R.drawable.ic_unliked).into(btnLikeHomeItem)
                    val uid=userPrefe.uid
                    updateFavoritesUserUnlike(uid, wear.name.trim(),onSuccess = {
                        context.getString(R.string.unliked).toast(WeakReference(context))
                        arreglo.remove(wear.name.trim())
                        userPrefe.favorites=arreglo
                        saveStringPreferences(Gson().toJson(userPrefe), Constants.USER, WeakReference(context))
                    },onError = {
                        Toast.makeText(context,"There was an error", Toast.LENGTH_SHORT)
                    })
                }
            }
        }
    }
}
