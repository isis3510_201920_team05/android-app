package mobileclass.huestyles.view.feed



import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.nfc.NfcAdapter
import android.nfc.NfcEvent
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.navigation.fragment.findNavController
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import com.mobileclass.huestyles.Manifest

import com.mobileclass.huestyles.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.datail_prenda_info.*
import kotlinx.android.synthetic.main.fragment_detail_prenda.*
import mobileclass.huestyles.extensions.toast
import mobileclass.huestyles.model.*

import java.lang.ref.WeakReference

class DetailPrendaFragment : Fragment() {
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var currentWear: Wear
    private var TAG=""
    private var androidBeamAvailable = false
    private lateinit var nfcAdapter: NfcAdapter
    private val fileUris = mutableListOf<Uri>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_prenda, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        currentWear = arguments?.getSerializable(Constants.WEAR) as Wear

        val sizes = currentWear.sizes.split(",")
        sizes.forEach{
            when(it){
                "XS" -> textXSDescription.setText(R.string.sizeAvailable)
                "S" -> textSDescription.setText(R.string.sizeAvailable)
                "M" -> textMDescription.setText(R.string.sizeAvailable)
                "L" -> textLDescription.setText(R.string.sizeAvailable)
                "XL" -> textXLDescription.setText(R.string.sizeAvailable)
                "XXL" -> textXXLDescription.setText(R.string.sizeAvailable)
            }
        }
        textName.text  = currentWear.name
        val gson = Gson()
        val userPrefe = gson.fromJson(getStringPreferences(WeakReference(requireContext()),Constants.USER), User::class.java)
        val colorBlindType = userPrefe.colorBlindType

        println(" COLOR_BLIN :::&&& $colorBlindType")
        when(colorBlindType){
            "Deuteranopia" -> {
                textDescription.text = currentWear.description.get(1)
            }
            "Tritanopia" -> textDescription.text = currentWear.description.get(2)
            "Monochromatic" -> textDescription.text = currentWear.description.get(3)
            else -> textDescription.text = currentWear.description.get(0)
        }

        textPrice.text = "$ ${currentWear.price} COP"
        Picasso.with(context).load(currentWear.src).into(imgDetailPrenda)
        val tags = currentWear.tags.split(",")
        var chip:Chip
        tags.forEach{
            chip = Chip(requireContext())
            chip.text = it
            chip.chipCornerRadius = 16F
            chipGroupDetailPrenda.addView(chip)
        }

        firebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD, Constants.DETAIL_PRENDA_EVENT)
        firebaseAnalytics.run{logEvent(Constants.DETAIL_PRENDA_EVENT,bundle)}
        btnSeeMaps.setOnClickListener {
            if(ContextCompat.checkSelfPermission(requireActivity(),Manifest.permission.MAPS_RECEIVE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(arrayOf(Manifest.permission.MAPS_RECEIVE),Constants.MY_PERMISSIONS_REQUEST_MAPS_RECEIVE)
            }else{
                saveStringPreferences(currentWear.stores.toString(), Constants.WEAR_STORES, WeakReference(it.context))
                findNavController().navigate(R.id.mapsActivity, bundle)
            }

        }
        imgDetailPrenda.setOnClickListener {
            val dialog = FullscreenImagePrenda()
            val bundle = Bundle()
            bundle.putString(Constants.WEAR_SRC,currentWear.src)
            dialog.arguments = bundle
            dialog.show(requireFragmentManager(),"fullscreenImage")
        }

        btnShare.setOnClickListener{
            if(ContextCompat.checkSelfPermission(requireActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),Constants.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE)
            }else{
                shareData()
            }
        }

        //Share with NFC
        val packageManager = requireActivity().packageManager
        androidBeamAvailable = if (!packageManager.hasSystemFeature(PackageManager.FEATURE_NFC)) {
            getString(R.string.noNFC).toast(WeakReference( requireContext()))
            false
            // Android Beam file transfer isn't supported
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getString(R.string.noNFCSupport).toast(WeakReference( requireContext()))
            false
        } else {
            // Android Beam file transfer is available, continue
            true
        }
        if(androidBeamAvailable){
            if(ContextCompat.checkSelfPermission(requireActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),Constants.PERMISSION_NFC_WRITE)
            }else{
                shareWithNFC()
            }
        }

        super.onActivityCreated(savedInstanceState)
    }

    private fun shareData(){
        val bitMapDrawable = imgDetailPrenda.drawable
        val bitMap = bitMapDrawable.toBitmap()
        val imgBitmapPath = MediaStore.Images.Media.insertImage(requireContext().contentResolver,bitMap,"title",null)
        val imgBitmapUri = Uri.parse(imgBitmapPath)
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        shareIntent.putExtra(Intent.EXTRA_STREAM,imgBitmapUri)
        shareIntent.putExtra(Intent.EXTRA_TEXT,R.string.lookThisGreatWear)
        shareIntent.type = "text/plain"
        startActivity(Intent.createChooser(shareIntent,"Share your clothes using"))
    }

    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<out String>,
        grantResults: IntArray) {
        when(requestCode){
            Constants.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE ->{
                if ((grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED)) {
                    shareData()
                } else {
                    getString(R.string.noPermissionGranted).toast(WeakReference( requireContext()))
                }
                return
            }
            Constants.PERMISSION_NFC_WRITE -> {
                if ((grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED)) {
                    shareWithNFC()
                } else {
                    getString(R.string.noPermissionGranted).toast(WeakReference( requireContext()))
                }
            }
            else ->{

            }
        }
    }

    private fun shareWithNFC(){
        nfcAdapter = NfcAdapter.getDefaultAdapter(requireContext())
        nfcAdapter.apply { nfcAdapter.setBeamPushUrisCallback(FileUriCallback(),requireActivity()) }
        val bitMapDrawable = imgDetailPrenda.drawable
        val bitMap = bitMapDrawable.toBitmap()
        val imgBitmapPath = MediaStore.Images.Media.insertImage(requireContext().contentResolver,bitMap,"title",null)
        imgBitmapPath?.run {
            val imgBitmapUri = Uri.parse(imgBitmapPath)
            fileUris += imgBitmapUri
        }
    }

    private inner class FileUriCallback: NfcAdapter.CreateBeamUrisCallback{
        override fun createBeamUris(event: NfcEvent?): Array<Uri> =
            fileUris.toTypedArray()
    }
}
