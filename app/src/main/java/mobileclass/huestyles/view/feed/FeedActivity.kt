package mobileclass.huestyles.view.feed

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import com.mobileclass.huestyles.R
import com.mobileclass.huestyles.R.layout.activity_feed

import kotlinx.android.synthetic.main.activity_feed.*
import kotlinx.coroutines.*
import mobileclass.huestyles.extensions.toast
import mobileclass.huestyles.model.*
import java.io.File
import java.lang.ref.WeakReference
import kotlin.coroutines.CoroutineContext

class FeedActivity : AppCompatActivity(), CoroutineScope {

    private lateinit var navController: NavController
    private val TAG = "Usuario"
    private var parentPath:File? = null
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job
    private lateinit var job: Job

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var connManager: ConnectivityManager

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(activity_feed)
        job = Job()
       launch{
           init()
       }
    }

    private fun init(){
        firebaseAnalytics = FirebaseAnalytics.getInstance(this@FeedActivity)
        val gson = Gson()
            val userPrefe = gson.fromJson(getStringPreferences(WeakReference(this@FeedActivity),Constants.USER),User::class.java)
        val bundle = Bundle()
        bundle.putString(Constants.KEY_EMAIL, userPrefe.email)
        bundle.putString(Constants.KEY_UID, userPrefe.uid)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN,bundle)
        connManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if(!userPrefe.onBoardingFinished){
            Snackbar.make(bottomNavigationView,R.string.onboardingDoNotFinished,Snackbar.LENGTH_INDEFINITE).setAction("OK"){
                Snackbar.make(bottomNavigationView,R.string.goToProfileToChange,Snackbar.LENGTH_INDEFINITE).setAction("OK"){}.show()
            }.show()
        }

        navController = Navigation.findNavController(this,R.id.fragmentContent)
        navController.navigateUp()
        bottomNavigationView.setupWithNavController(navController)
    }

    private fun handleIntent(){
        if (TextUtils.equals(intent.action, Intent.ACTION_VIEW)) {
            // Get the URI from the Intent
            intent.data?.also { beamUri ->
                /*
                 * Test for the type of URI, by getting its scheme value
                 */
                parentPath = when (beamUri.scheme) {
                    "file" -> handleFileUri(beamUri)
                    "content" -> handleContentUri(beamUri)
                    else -> null
                }
            }
        }
    }

    private fun handleFileUri(beamUri: Uri): File?{
        "handleFileUri".toast(WeakReference( this@FeedActivity))
        return beamUri.path.let { fileName ->
            // Create a File object for this filename
            File(fileName)
                // Get the file's parent directory
                .parentFile
        }
    }

    private fun handleContentUri(beamUri: Uri): File?{
        "handleContentUri".toast(WeakReference( this@FeedActivity))
        return if (beamUri.authority == MediaStore.AUTHORITY) {
            /*
             * Handle content URIs for other content providers
             */
            // For a MediaStore content URI
            "Handle content URIs fir other content providers".toast(WeakReference( this@FeedActivity))
            null
        } else {
            "Get the column that contains the file name".toast(WeakReference( this@FeedActivity))
            // Get the column that contains the file name
            val projection = arrayOf(MediaStore.MediaColumns.DATA)
            val pathCursor = contentResolver.query(beamUri, projection, null, null, null)
            // Check for a valid cursor
            if (pathCursor?.moveToFirst() == true) {
                // Get the column index in the Cursor
                pathCursor.getColumnIndex(MediaStore.MediaColumns.DATA).let { filenameIndex ->
                    // Get the full file name including path
                    val parent = pathCursor.getString(filenameIndex).let { fileName ->
                        // Create a File object for the filename
                        File(fileName)
                    }.parentFile // Return the parent directory of the file
                    pathCursor.close()
                    return parent
                }
            } else {
                pathCursor.close()
                " The query didn't work; return null".toast(WeakReference( this@FeedActivity))
                // The query didn't work; return null
                null
            }
        }
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }

}