package mobileclass.huestyles.view.feed


import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.mobileclass.huestyles.R
import kotlinx.android.synthetic.main.content_home_content.view.*
import kotlinx.android.synthetic.main.feed_home_item.*
import kotlinx.android.synthetic.main.fragment_favorites.*
import listeners.RecyclerWearListener
import mobileclass.huestyles.extensions.doIfInternetConnection
import mobileclass.huestyles.extensions.goDetailPrenda
import mobileclass.huestyles.model.Constants
import mobileclass.huestyles.model.User
import mobileclass.huestyles.model.getStringPreferences
import mobileclass.huestyles.model.Wear
import java.lang.ref.WeakReference

/**
 * A simple [Fragment] subclass.
 */
class FavoritesFragment : Fragment() {

    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private lateinit var _view:View
    private lateinit var clothingDbRef: CollectionReference
    private val list:ArrayList<Wear> = ArrayList()
    private lateinit var recycler: RecyclerView
    private lateinit var adapter: WearAdapter
    private var TAG=""
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var connManager: ConnectivityManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _view = inflater.inflate(R.layout.fragment_favorites, container, false)
        setUpClothingDB()
        recycler = _view.recycler as RecyclerView
        setRecyclerView()
        getFavorites()
        return _view
    }

    private fun setRecyclerView() {
        recycler.setHasFixedSize(true)
        recycler.itemAnimator= DefaultItemAnimator()
        val layoutManager = GridLayoutManager(context,2)
        adapter=(WearAdapter(list, object : RecyclerWearListener {

            override fun onLike(wear: Wear, adapterPosition: Int){
                doIfInternetConnection(WeakReference(textNameHomeItem),WeakReference(connManager)){
                    btnLikeHomeItem.setOnClickListener{
                    }
                }
            }

            override fun onClick(wear: Wear, position: Int) {
                doIfInternetConnection(WeakReference(textNameHomeItem),WeakReference(connManager)){
                    val bundle = Bundle()
                    bundle.putSerializable(Constants.WEAR,wear)
                    findNavController().goDetailPrenda(bundle)
                }
            }

        }))
        recycler.layoutManager= layoutManager
        recycler.adapter=adapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Progresso.visibility=View.VISIBLE
        super.onActivityCreated(savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())
        connManager = requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD,Constants.CATEGORIES_EVENT)
        firebaseAnalytics.run{logEvent(Constants.CATEGORIES_EVENT,bundle)}
        fragmentManager?.apply { this.beginTransaction().addToBackStack("favoritesFragment")}
    }

    private fun setUpClothingDB() {
        clothingDbRef = db.collection("clothing")
    }

    private fun getFavorites(){
        val gson = Gson()
        val userPrefe = gson.fromJson(getStringPreferences(WeakReference(requireContext()), Constants.USER), User::class.java)
        val favs:List<String> = userPrefe.favorites
        for(fav in favs){
            clothingDbRef.whereEqualTo("name", fav).get()
                .addOnSuccessListener { result  ->
                    for (document in result ) {
                        val stores = ArrayList<Int>()
                        stores.addAll(document.get("stores") as Collection<Int>)
                        val wear = document.toObject(Wear::class.java)
                        list.add(wear)
                        adapter.notifyDataSetChanged()
                        recycler.smoothScrollToPosition(list.size)
                    }
                    Progresso.visibility=View.GONE  
                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error getting documents: ", exception)
                }
        }

    }

}
