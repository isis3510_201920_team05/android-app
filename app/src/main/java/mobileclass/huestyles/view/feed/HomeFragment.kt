package mobileclass.huestyles.view.feed
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import com.mobileclass.huestyles.R.layout.*
import mobileclass.huestyles.model.Wear
import com.google.firebase.analytics.FirebaseAnalytics
import android.util.Log
import androidx.recyclerview.widget.DefaultItemAnimator
import com.google.firebase.firestore.CollectionReference
import kotlinx.android.synthetic.main.content_home_content.view.*
import listeners.RecyclerWearListener
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.feed_home_item.*
import kotlinx.coroutines.*
import mobileclass.huestyles.extensions.goDetailPrenda
import mobileclass.huestyles.model.Constants
import mobileclass.huestyles.model.Constants.HOME_EVENT

import mobileclass.huestyles.model.User
import mobileclass.huestyles.model.getStringPreferences
import mobileclass.huestyles.model.updateFavoritesUser
import mobileclass.huestyles.model.updateUser
import android.R
import android.os.Handler
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.google.firebase.firestore.model.Document
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_home_content.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import mobileclass.huestyles.view.feed.OnBottomReachedListener

import mobileclass.huestyles.extensions.doIfInternetConnection
import java.lang.ref.WeakReference
import kotlin.coroutines.CoroutineContext


class HomeFragment : Fragment(), CoroutineScope{

    private val db:FirebaseFirestore= FirebaseFirestore.getInstance()
    private lateinit var _view:View
    private lateinit var clothingDbRef: CollectionReference
    private val mainList:ArrayList<Wear> = ArrayList()
    private val allList:ArrayList<Wear> = ArrayList()
    private lateinit var recycler:RecyclerView
    private lateinit var adapter: WearAdapter
    private var TAG=""
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var connManager: ConnectivityManager
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var tempDocument: QueryDocumentSnapshot
    var isLoading=false
    val limit =20

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job
    private lateinit var job: Job

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        job = Job()
        _view = inflater.inflate(fragment_home, container, false)
        setUpClothingDB()
        recycler = _view.recycler as RecyclerView
        setRecyclerView()
        getClothes()

        return _view
    }

    private fun setRecyclerView(){
        recycler.setHasFixedSize(true)
        recycler.itemAnimator=DefaultItemAnimator()
        val layoutManager = GridLayoutManager(context,2)
        adapter=(WearAdapter(mainList, object : RecyclerWearListener {
            override fun onLike(wear: Wear, adapterPosition: Int){
                doIfInternetConnection(WeakReference(textNameHomeItem),WeakReference(connManager)){
                    btnLikeHomeItem.setOnClickListener{
                    }
                }
            }
            override fun onClick(wear: Wear, position: Int) {
                doIfInternetConnection(WeakReference(textNameHomeItem),WeakReference(connManager)){
                    val bundle = Bundle()
                    bundle.putSerializable(Constants.WEAR,wear)
                    findNavController().goDetailPrenda(bundle)
                }
            }
        }))
        recycler.layoutManager= layoutManager
        recycler.adapter=adapter
    }

    private fun getClothes() {
        isLoading = true
        launch {
            var i = 0
            clothingDbRef.limit(20).get()
                .addOnSuccessListener { result ->
                    for (document in result) {
                        val stores = ArrayList<Int>()
                        stores.addAll(document.get("stores") as Collection<Int>)
                        val wear = document.toObject(Wear::class.java)
                        allList.add(wear)
                        i++
                        mainList.add(wear)
                        adapter.notifyDataSetChanged()
                        if (i == limit - 1) {
                            tempDocument = document
                        }
                    }
                    isLoading = false
                    recycler.adapter = adapter
                    Progress?.visibility=View.GONE
                    btn20More?.visibility=View.VISIBLE
                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error getting documents: ", exception)
                }
        }
    }
    private fun getPage(){
        btn20More.visibility=View.GONE
        Progress.visibility=View.VISIBLE
        isLoading=true
        clothingDbRef.startAfter(tempDocument).limit(20).get()
            .addOnSuccessListener { result ->
                var i = 0
                for (document in result) {
                    val stores = ArrayList<Int>()
                    stores.addAll(document.get("stores") as Collection<Int>)
                    val wear = document.toObject(Wear::class.java)
                    mainList.add(wear)
                    i++
                    if(i==limit-1){
                        tempDocument=document
                    }
                }
                Log.w(TAG, "entra: " + mainList.size)
                Handler().postDelayed({
                    adapter.notifyDataSetChanged()
                    isLoading=false
                    Progress.visibility=View.GONE
                    btn20More.visibility=View.VISIBLE
                },5000)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }
    private fun setUpClothingDB() {
        clothingDbRef = db.collection("clothing")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        btn20More.visibility=View.GONE
        Progress.visibility=View.VISIBLE
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD,HOME_EVENT)
        connManager = requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        firebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())
        firebaseAnalytics.logEvent(HOME_EVENT,savedInstanceState)
        super.onActivityCreated(savedInstanceState)
        btn20More.setOnClickListener {
            //TODO filter by the user preferences before startFeed
            doIfInternetConnection(WeakReference(it),WeakReference(connManager)) {
                if(!isLoading){
                    getPage()
                }
            }
        }
    }

    override fun onStop() {
        job.cancel()
        super.onStop()
    }
}



