package listeners

import mobileclass.huestyles.model.Wear

interface RecyclerWearListener{
    fun onClick(wear: Wear, position:Int)
    fun onLike(wear: Wear, adapterPosition: Int)
}